## Modul 6 - Course 1

 `Quellen: Zusammengefasst ChatGPT und Course 1`

[TOC]

---

### Zweck

In diesem Modul werden die Vorteile und Funktionen von Amazon Elastic Block Store (Amazon EBS) erläutert. Anschließend werden reale Fallstudien zu Amazon EBS analysiert. Abschließend erstellen die Teilnehmer ihre eigenen EBS-Volumes und binden sie an eine Amazon Elastic Compute Cloud (Amazon EC2)-Instanz an.

### Technologische Begriffe
---
Um mehr über die in diesem Modul verwendete Technologie-Terminologie zu erfahren, erweitern Sie jede der folgenden fünf Kategorien.

- **Amazon Elastic Block Store (Amazon EBS)** - Speicher für bestimmte Amazon Elastic Compute Cloud (Amazon EC2)-Instanzen. Denken Sie daran wie an die Festplatte für Ihre EC2-Instanz.

- **Amazon Elastic Compute Cloud (Amazon EC2)** - Ein Webdienst, der sichere, skalierbare Rechenkapazität in der Cloud bereitstellt. Denken Sie daran, dass Sie in der Cloud einen Computer mieten.

- **Festplattenlaufwerk (HDD)** - Langsamere Speicherung, die eine rotierende Festplatte zur Datenspeicherung verwendet.

- **Input/Output Operations Per Second (IOPS)** - Eine gängige Leistungsmessung, die zur Benchmarking von Computerspeichergeräten wie Festplattenlaufwerken (HDDs) und Solid State Drives (SSDs) verwendet wird.

- **Solid State Drive (SSD)** - Sehr schneller Speicher, der Flash-Speicher anstelle einer rotierenden Festplatte verwendet.

### Hintergrund und Missverständnisse
---
Amazon EBS ist Speicher für EC2-Instanzen mit wichtigen Vorteilen:

- Datenverfügbarkeit
- Datenbeständigkeit
- Datenverschlüsselung
- Snapshots

Amazon EBS-Speicher ist als eine Reihe von festen Blöcken implementiert, die vom Betriebssystem gelesen und geschrieben werden können. Es wird nichts darüber gespeichert, was diese Blöcke repräsentieren oder welche Attribute sie haben. Die Blöcke ähneln stark den Dateisystemen New Technology File System (NTFS) oder File Allocation Table (FAT), die auf Ihrem PC oder Mac laufen. Das bedeutet, dass sie schnell abgerufen werden können.

Amazon S3-Speicher wird als ein Objekt implementiert, das von der Anwendung, die das Objekt verwendet, gelesen und geschrieben werden muss. Objekte enthalten Metadaten - Daten über die Attribute des Objekts, die dem System bei der Katalogisierung und Identifizierung des Objekts helfen. Beispiele für Objekte sind Bilder, Videos und Musik. Objekte können nicht inkrementell verarbeitet werden. Sie müssen in ihrer Gesamtheit gelesen und geschrieben werden. Dies kann Leistungs- und Konsistenzprobleme verursachen.

Es gibt mehrere andere Unterschiede zwischen Amazon S3 und Amazon EBS-Speicher, einschließlich Unterschieden in den Kosten, der Durchsatzleistung und der Performance. Diese Unterschiede werden auf der Webseite [Cloud Storage on AWS](https://aws.amazon.com/de/storage/) näher erläutert. Es liegt in der Verantwortung des Benutzers oder Anwendungsentwicklers zu entscheiden, ob Amazon S3 oder Amazon EBS-Speicher für eine bestimmte Anwendung geeigneter ist.

Es gibt zwei Haupttypen von EBS-Volumes, und jeder Haupttyp hat zwei Untertypen. Jeder Typ hat Vor- und Nachteile, daher ist es wichtig, den Typ auszuwählen, der am besten zur durchzuführenden Arbeit passt.

Weitere Informationen zu EBS-Volumenarten finden Sie auf der [AWS-Webseite](https://aws.amazon.com/de/ebs/volume-types/).

#### wichtige Unterschiede zwischen Amazon S3 und Amazon EBS-Datenspeicher

---

- Amazon EBS kann nur verwendet werden, wenn es an eine EC2-Instanz angeschlossen ist. Im Gegensatz dazu kann auf Amazon S3 eigenständig über Hypertext Transfer Protocol (HTTP)-Protokolle zugegriffen werden.

- Amazon EBS kann nicht so viele Daten wie Amazon S3 speichern.

- Amazon EBS kann nur an eine EC2-Instanz angehängt werden, während Daten in einem S3-Bucket von mehreren EC2-Instanzen aus abgerufen werden können.

- Amazon S3 hat mehr Verzögerungen beim Schreiben von Daten als Amazon EBS.

- EBS-Volumes werden insgesamt verschlüsselt, während Amazon S3-Objekte individuell durch serverseitige Verschlüsselung (SSE) verschlüsselt werden.

- Amazon EBS umfasst drei Arten von Volumes, während Amazon S3 mehr Typen umfasst: S3 Standard, S3 Standard-Infrequent Access (S3 Standard-IA), S3 One Zone-Infrequent Access (S3 One Zone-IA), S3 Intelligent-Tiering, S3 Glacier, S3 Glacier Deep Archive.
