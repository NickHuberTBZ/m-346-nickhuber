## Modul 5 - Course 1

 `Quellen: Zusammengefasst ChatGPT und Course 1`

[TOC]

---
### Zweck

Dieser Kurs bietet einen tieferen Einblick in das Thema Cloud Computing und den Cloud-Computing-Dienstanbieter Amazon Web Services (AWS). Sie werden AWS-Dienste und -Technologien erkunden und erfahren, wie sie dazu verwendet werden, Unternehmen auf der ganzen Welt zu unterstützen. Sie werden AWS-Technologien verwenden, indem Sie in der AWS Management Console arbeiten, erfolgreiche Cloud-Implementierungen untersuchen und Ihr Wissen auf cloudbasierte Szenarien anwenden.

### Technologische Begrifflichkeiten
---

**Amazon CloudFront** – Ein schneller Content-Delivery-Netzwerkdienst (CDN), der Daten, Videos, Anwendungen und Anwendungsprogrammierschnittstellen (APIs) sicher weltweit mit geringer Latenz und hoher Übertragungsgeschwindigkeit in einer entwicklerfreundlichen Umgebung bereitstellt.

**AWS Direct Connect** – Direct Connect ist eine Cloud-Service-Lösung, die es ermöglicht, eine dedizierte Netzwerkverbindung von Ihrer On-Premises-Umgebung zu AWS herzustellen. Mit Direct Connect können Sie eine private Verbindung zwischen AWS und Ihrem Rechenzentrum, Büro oder Colocation-Umgebung herstellen, was in vielen Fällen Ihre Netzwerkkosten reduzieren, die Bandbreitendurchsatz erhöhen und eine konsistentere Netzwerkerfahrung bieten kann als internetbasierte Verbindungen.

**Caching** – Das Speichern häufig angeforderter Daten an Edge-Standorten, um auf sie schneller zugreifen zu können.

**Content Delivery Network (CDN)** – Ein System verteilter Server (Netzwerk), das Seiten und andere Webinhalte basierend auf den geografischen Standorten des Benutzers, dem Ursprung der Webseite und dem Content-Delivery-Server an den Benutzer liefert.

**Distribution** – Gibt CloudFront an, wo es die Informationen abrufen soll, die es an den Edge-Standorten zwischenspeichert, und wie es die Content-Delivery überwacht und verwaltet.

**Edge-Standort** – Ein Ort, an dem Daten für geringere Latenz gespeichert werden können. Oft sind Edge-Standorte in der Nähe von dicht besiedelten Gebieten, die hohe Verkehrsvolumina erzeugen werden.

**Ursprung** – Ein komplexer Typ, der den Amazon S3-Bucket, den Hypertext Transfer Protocol (HTTP)-Server (z. B. einen Webserver) oder einen anderen Server beschreibt, von dem CloudFront Ihre Dateien abruft.

### Hintergrund und Missverständnisse

----

Aufgrund der Natur des Webs werden bestimmte Inhalte häufiger angefordert und müssen nahezu sofort geliefert werden. Eine Website, deren Ursprung auf einem Server in Kalifornien liegt, könnte etwas hosten, das in Schweden viral geht. In diesem Fall wird es einen plötzlichen Ansturm von Anfragen nach diesen Daten geben, was zu erhöhter Latenz führen und die Website sogar zum Absturz bringen könnte, wenn der Server den Traffic nicht bewältigen kann.

CloudFront arbeitet mit den Edge-Standorten zusammen, die Teil der AWS Globalen Infrastruktur sind. Zusammen ermöglichen sie, dass häufig angeforderte Daten an den Edge-Standorten zwischengespeichert werden. Während die erste Anfrage CloudFront dazu veranlasst, die Datei in den Cache zu laden, können nachfolgende Anfragen viel schneller erfüllt werden, und ein Teil der Arbeit kann vom Ursprungsserver abgeleitet werden.

Dieser Prozess kann auch auf lokalerer Ebene auf der Amazon.com-Startseite beobachtet werden. Produkte, die auf der Startseite angezeigt werden, können zwischengespeichert werden, da jede Person, die die Amazon.com-Website besucht, automatisch diese Produkte vom Server anfordert, was zu Verzögerungen führen könnte. Das Zwischenspeichern ermöglicht es, diese Startseitenprodukte an Edge-Standorten für einen schnelleren Zugriff zu speichern. Die übrigen Produkte können auf dem Ursprungsserver gespeichert bleiben, da sie seltener angefordert werden und eine leicht höhere Latenz tolerieren können.

Das Zwischenspeichern an den Edge-Standorten ist nicht permanent, und alle zwischengespeicherten Daten haben eine "Time to Live" (TTL), die angibt, wie lange sie zwischengespeichert werden.

### Aufgaben des Modules

Im Rahmen dieser Aufgabe habe ich einen Amazon S3-Bucket eingerichtet und eine vollständige Website darauf hochgeladen. Dies erforderte das Hochladen von HTML-, CSS- und anderen Dateien, um eine funktionierende Website in der Cloud zu hosten. Dabei konnte ich praktische Erfahrung im Umgang mit Amazon S3 sammeln und die theoretischen Konzepte des Kurses in die Praxis umsetzen. Das HTML-, CSS-File habe ich einfach simpel von ` ChatGPT ` generieren lassen. 
