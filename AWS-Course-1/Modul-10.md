## Modul 10 - Course 1

## `Quellen: Zusammengefasst ChatGPT und Course 1`

[TOC]

---

## Zweck

Das Ziel dieses Moduls ist es, Ihnen das Amazon Relational Database Service (Amazon RDS), Amazon DynamoDB und Data Warehousing mit Amazon Redshift näherzubringen. Außerdem werden Sie relationale und nichtrelationale Datenbanken sowie Online-Transaktionsverarbeitung (OLTP) und Online-Analyseverarbeitung (OLAP) vergleichen.

## Modulbeschreibung

In diesem Modul werden Sie je nach Szenario eine relationale oder nichtrelationale Datenbank empfehlen. Sie werden eine RDS-DB-Instanz erstellen und die angemessene Verwendung von relationalen und nichtrelationalen Datenbanksystemen kennenlernen und besprechen.

## Technologische Terminologie

Um mehr über die technologische Terminologie in diesem Modul zu erfahren, erweitern Sie jede der folgenden neun Kategorien.

### Relationale Datenbank

Eine Sammlung von Datensätzen, die als Datensätze und Spalten in Tabellen organisiert sind. In einem relationalen Datenbanksystem sind Beziehungen zwischen den Datenbanktabellen definiert. Denken Sie an eine relationale Datenbank als eine Menge von Daten mit 1-zu-1 und 1-zu-vielen Beziehungen. Entwickler verwenden strukturierte Abfragesprache (SQL), um mit der Datenbank zu interagieren.

### Amazon Relational Database Service (Amazon RDS) 

Amazon RDS ermöglicht Entwicklern das Erstellen und Verwalten von relationalen Datenbanken in der Cloud. Amazon RDS ermöglicht es Entwicklern, große Datenmengen zu verfolgen und effizient zu organisieren und durchsuchen.

### Amazon DynamoDBm 

Der AWS nichtrelationale Datenbankdienst. Daten werden in Schlüssel-Wert-Paaren gespeichert.

### Nichtrelationale Datenbank

Auch als "NoSQL" oder "Nicht nur SQL" Datenbank bezeichnet. Jeder Eintrag wird in einem Schlüssel-Wert-Paar gespeichert, bei dem jedem Schlüssel Werte zugeordnet sind. Jeder Eintrag kann eine unterschiedliche Anzahl von Werten haben.

### Amazon Redshift

Der AWS Data-Warehousing-Dienst, der massive Datenmengen auf eine Art und Weise speichern kann, die schnelle Abfragen für Business Intelligence (BI)-Zwecke ermöglicht.

### Online-Transaktionsverarbeitung (OLTP)

Eine Kategorie der Datenverarbeitung, die sich auf transaktionsorientierte Aufgaben konzentriert. OLTP umfasst in der Regel das Einfügen, Aktualisieren oder Löschen kleiner Datenmengen in einer Datenbank.

### Online-Analyseverarbeitung (OLAP)

Eine Methode der Datenverarbeitung, die es Benutzern ermöglicht, Daten effizient und selektiv zu extrahieren und abzufragen, um sie aus verschiedenen Blickwinkeln zu analysieren.

### Amazon Aurora

Ein relationales Datenbankmanagementsystem, das mit MySQL und PostgreSQL kompatibel ist und für die Cloud entwickelt wurde. Es kombiniert die Leistung und Verfügbarkeit von traditionellen Unternehmensdatenbanken mit der Einfachheit und Wirtschaftlichkeit von Open-Source-Datenbanken.

### MySQL

Ein Open-Source-Relationales Datenbankmanagementsystem.

## Hintergrund und Missverständnisse

### OLTP und OLAP

Es stehen viele verschiedene Arten von Datenbanken zur Verfügung. Um festzulegen, welche Art von Datenbank Sie benötigen, ist es wichtig zu wissen, wie die Daten verarbeitet werden. Es gibt zwei Arten von Datenverarbeitung: Online-Transaktionsverarbeitung (OLTP) und Online-Analyseverarbeitung (OLAP).

#### OLAP-Operationen

OLAP-Operationen sind hauptsächlich schreibgeschützt. Sie lesen die Daten und führen verschiedene Arten der Aggregation wie Summieren, Gruppieren und Sortieren durch. Relationale Datenbankverwaltungssysteme haben eingebaute Funktionen für diese Art von Operationen. Da sie eingebaut sind, werden sie effizient durchgeführt. In einer nichtrelationalen Datenbank müssen die Werte aus den Schlüssel-Wert-Paaren extrahiert werden, was einen zeitaufwändigen Prozess darstellen kann.

#### OLAP-Systeme

OLAP-Systeme werden oft dort verwendet, wo das System dazu verpflichtet ist, viele verwandte Daten zu verarbeiten, möglicherweise zur Generierung von Geschäftsberichten. Unternehmen müssen oft eine große Anzahl von Datenpunkten analysieren, die über einen langen Zeitraum aufgetreten sind, um Trends zu erkennen und Verhaltensweisen vorherzusagen.

Diese Art von System muss nicht unbedingt ein Echtzeitsystem sein, es kann als Hintergrundprozess ausgeführt werden. In einem E-Commerce-System könnte beispielsweise ein OLAP-System im Hintergrund laufen, ohne die Benutzererf

ahrung zu beeinträchtigen. Heutzutage sieht man eher relationale Datenbanken (insbesondere große, spaltenorientierte Datenspeicher), anstatt nichtrelationale Datenbanken, die für OLAP verwendet werden.

#### OLTP-Operationen

OLTP-Operationen müssen jedoch die Datenbank neben dem Lesen auch aktualisieren. Das Aktualisieren kann das Hinzufügen, Ändern oder Löschen von Werten umfassen. Das Aktualisieren kann komplex werden, da viele Tabellen in einer relationalen Datenbank virtuell sind. Das bedeutet, dass die Tabellen in Echtzeit aus nichtvirtuellen Tabellen kombiniert werden müssen. Betrachten Sie das folgende Beispiel.

Eine Kaufhausdatenbank enthält Tabellen mit Informationen zu Kunden und Produkten. Die Kundentabelle enthält nur Daten zu Kunden wie Namen und Adressen. Die Produkttabelle enthält nur Daten zu Produkten wie Name und Preis. Um Informationen über Einkäufe zu erfassen, muss eine Kauf-Tabelle erstellt werden, die einen kombinierten Primärschlüssel enthält, der sowohl die Kunden-ID als auch die Produkt-ID enthält und anzeigt, wie viel von einem bestimmten Produkt ein bestimmter Kunde gekauft hat.

Um eine vollständige Ausgabe des Kaufs anzuzeigen, müssen die Kundentabelle und die Produkttabelle in Echtzeit mit der Kauf-Tabelle kombiniert werden, um Dinge wie den Kundennamen, den Produktnamen, die Menge des gekauften Produkts und den Verkaufspreis anzuzeigen. Die Art der Operation, die Tabellen in Echtzeit kombiniert, wird als JOIN bezeichnet. Das Ergebnis eines JOIN ist eine virtuelle Tabelle und in den meisten Fällen kann sie nicht direkt aktualisiert werden.

OLTP-Systeme werden oft dort verwendet, wo das System dazu verpflichtet ist, große Volumina von Transaktionen mit hoher Geschwindigkeit zu verarbeiten. Viele E-Commerce-Systeme, wie Einkaufswagen, verkaufen während des Kassenvorgangs eine große Anzahl von Artikeln, während sie gleichzeitig die Artikel aus der Inventartabelle entfernen. Wenn die Integrität der gesamten Transaktion wichtig ist und die Verarbeitung in nahezu Echtzeit erfolgen muss, sollten Unternehmen OLTP-Systeme in Betracht ziehen.

OLTP-Systeme sind nicht ausschließlich relationale Datenbanken, obwohl es in den Daten Beziehungen gibt. Es wird immer häufiger, dass nichtrelationale Datenbanken Einschränkungen durchsetzen und Transaktionen ermöglichen, so dass diese Datenbanken als OLTP-Systeme verwendet werden können.

Schließlich müssen Integritätsüberlegungen in einer relationalen Datenbank behandelt werden. In dem Beispiel, wenn ein Produkt aus der Produkttabelle gelöscht werden muss, müssen Regeln vorhanden sein, um sicherzustellen, dass Referenzen auf das Produkt ebenfalls behandelt werden. Diese Arten von Regeln werden als Integritäts- und Konsistenzregeln bezeichnet.

## Vergleich von OLTP und OLAP

### OLTP
- **Verarbeitet aktuelle operative Daten**
- **Größe ist kleiner, typischerweise zwischen 100 MB und 10 GB**
- **Ziel ist die Durchführung von Tagesgeschäftsoperationen**
- **Verwendet einfache Abfragen**
- **Schnellere Verarbeitungsgeschwindigkeiten**
- **Erfordert Lese-/Schreiboperationen**

### OLAP
- **Verarbeitet alle historischen Daten**
- **Größe ist größer, typischerweise zwischen 1 TB und 100 PB**
- **Ziel ist es, Entscheidungen aus großen Datenquellen zu treffen**
- **Verwendet komplexe Abfragen**
- **Langsamere Verarbeitungsgeschwindigkeiten**
- **Erfordert nur Leseoperationen**

### Anwendungen von OLTP
- **Online-Bestellungen erfassen**
- **Einkäufe verarbeiten**
- **Kundeninformationen speichern**

### Anwendungen von OLAP
- **Analyse von Einkaufsmustern zur Empfehlung von Produkten**
- **Verfolgung von Kauftrends für gezielte Werbung**
- **Analyse saisonaler Kauftrends, um sicherzustellen, dass Artikel vorrätig sind**
