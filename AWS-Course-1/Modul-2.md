## Modul 2 - Course 1

### `Quellen: Zusammengefasst ChatGPT und Course 1`

[TOC]

---

#### Zweck

Das Ziel dieses Moduls ist es, die drei verschiedenen Arten von Cloud-Services zu verstehen: Infrastructure as a Service (IaaS), Platform as a Service (PaaS) und Software as a Service (SaaS). Außerdem wirst du etwas über die geografische Struktur der Amazon Web Services (AWS) Cloud-Infrastruktur lernen, einschließlich Regionen, Verfügbarkeitszonen und Edge-Standorte.

#### Modulbeschreibung

Dieses Modul wird aus Notizen und Recherchen bestehen. Du wirst auch Diagramme der globalen AWS-Infrastruktur erstellen. Du wirst reale Beispiele für jede Art von Cloud-Computing-Service finden.

#### Technologische Terminologie

Um mehr über die in diesem Modul verwendete technologische Terminologie zu erfahren, erweitere jede der folgenden sieben Kategorien.

>###### **Verfügbarkeitszone** 
>Ein oder mehrere Rechenzentren, die viele Server beherbergen. Jede Region verfügt über mehrere isolierte Standorte, die als Verfügbarkeitszonen bezeichnet werden. Jede Verfügbarkeitszone ist isoliert, aber die Verfügbarkeitszonen in einer Region sind durch Low-Latency-Verbindungen verbunden. Eine Verfügbarkeitszone wird durch einen Regionscode gefolgt von einem Buchstabenkennzeichen dargestellt, zum Beispiel us-east-1a.
>
> ###### **Edge-Standort**
> Ein Ort, an dem Daten zur Verringerung der Latenz gespeichert werden können. Oft befinden sich Edge-Standorte in der Nähe von dicht besiedelten Gebieten, die hohe  Verkehrsvolumen generieren werden.
>
> ###### **Infrastructure as a Service (IaaS)**
> Ein Modell, bei dem virtuelle Maschinen und Server verwendet werden, um Kunden das Hosting einer Vielzahl von Anwendungen zu ermöglichen, und IT-Services werden bereitgestellt.
>
>###### **Latenz**
>Die Verzögerung, bevor ein Datentransfer beginnt, nachdem die Daten angefordert wurden.
>
>###### **Platform as a Service (PaaS)** 
>Ein Modell, das eine virtuelle Plattform bereitstellt, mit der Kunden benutzerdefinierte Software erstellen können.
>
>###### **Region**
>Ein Bereich, in dem Daten gespeichert werden. Die Speicherung von Daten in einer Region in Ihrer Nähe ist einer der Gründe, warum sie in Lichtgeschwindigkeit abgerufen werden können.
>
>###### **Software as a Service (SaaS)** 
>Ein Modell, das Anwendungen über das Internet bereitstellt, die von einem Drittanbieter verwaltet werden.
>
#### Hintergrund und Missverständnisse

Die globale AWS-Cloud-Infrastruktur ist die sicherste, umfangreichste und zuverlässigste Cloud-Plattform und bietet über 200 voll ausgestattete Dienste aus Rechenzentren weltweit an. Diese Infrastruktur setzt sich aus vielen verschiedenen Komponenten zusammen, darunter Regionen, Verfügbarkeitszonen und Edge-Standorte. Die Unterschiede zwischen den Komponenten der Infrastruktur können verwirrend sein, da sie alle miteinander verbunden sind und sich auf die physische Struktur der AWS-Cloud beziehen. Es ist gut, ein konkretes visuelles Beispiel zu haben.

Region > Verfügbarkeitszone > Edge-Standort

Eine Ressource für die Arten von Cloud-Services findest du auf der Seite AWS Types of Cloud Computing.

>##### **IaaS**
>Diese Dienste enthalten die grundlegenden Bausteine der Cloud. Sie bieten Zugriff auf Computer - physische und virtuelle - sowie auf Netzwerkfunktionen und Speicherplatz. Denke an IaaS wie das Mieten einer Küche. Du kannst alle verschiedenen Geräte (Mixer, Mixer, Spülen) verwenden, und du kannst eine Küche mit besseren Geräten mieten, wenn du sie benötigst.
Beispiele: Amazon Elastic Compute Cloud (Amazon EC2), Rackspace, Google Compute Engine
>
>##### **PaaS** 
>Diese Dienste sind die Werkzeuge, die zur Verwaltung der zugrunde liegenden Hardware und zum Starten von Anwendungen benötigt werden. Sie umfassen Programmierumgebungen, Plattformen für Anwendungstests und Anwendungsstarter. Denke an PaaS wie an den Besuch eines Restaurants. Du verwaltest nicht die Geräte in der Küche, aber du kannst den Kellner oder den Koch bitten, Dinge so zuzubereiten, wie du es möchtest.
>Beispiele: AWS Elastic Beanstalk, Microsoft Azure, Google App Engine
>
>##### **SaaS** 
>Diese Dienste sind die tatsächlichen Apps und Software, die über das Internet bereitgestellt werden. Du bist nicht dafür verantwortlich, die Software zu verwalten oder zu installieren; du greifst nur darauf zu und nutzt sie. Denke an SaaS wie an das Essen in einem All-You-Can-Eat-Buffet. Du hast Zugriff auf das Essen, das serviert wird. Du kontrollierst nicht, was gemacht wird oder wie, aber du kannst so viel nutzen, wie du möchtest.
Beispiele: Dropbox, Slack, Spotify, YouTube, Microsoft Office 365, Gmail
>
#### Fokusfragen

Befolge die Anweisungen deines Lehrers, um die folgenden Fragen zu beantworten und zu diskutieren.

``1. Wie gelangt dein Computer an Informationen aus dem Internet? Wenn du eine Website öffnest, woher stammt die Website? Wer stellt die Daten zur Verfügung? Verwende dein Wissen über Informatik und Cloud Computing für deine Antwort.``
>Dein Computer fragt über das Internet Daten von einem Webserver ab, wenn du eine Website öffnest. Die Website besteht aus Dateien wie Text, Bildern und Code. Diese Daten werden auf dem Webserver gespeichert und an deinen Computer übertragen. Cloud-Computing-Dienste wie AWS können diese Daten in der Cloud speichern, was schnellen Zugriff und globale Verfügbarkeit ermöglicht.


``2. Gibt es ein Programm oder eine App, die du verwendest, die vollständig in der Cloud läuft, das heißt, du musst nichts auf deinem Computer oder Gerät speichern? Wofür verwendest du das Programm? Wie denkst du, wird dir das Programm kostenlos oder zu einem geringen Preis angeboten?``

>Ja, ein Beispiel für eine vollständig in der Cloud laufende Anwendung ist Google Docs. Es ermöglicht das Erstellen und Bearbeiten von Dokumenten ohne lokale Speicherung. Google bietet es oft kostenlos an, finanziert durch Anzeigen oder kostenpflichtige Modelle für zusätzliche Funktionen und Speicherplatz.

``3. Immer mehr Programme und Apps werden von der lokalen Speicherung auf individuellen Computern in die Cloud verschoben. Zum Beispiel nutzen viele Menschen internetbasierte Textverarbeitung anstelle von Software wie Microsoft Word und Spotify anstelle von CDs und MP3-Playern. Welches andere Programm oder welcher andere Dienst wird deiner Meinung nach in die Cloud verlagert? Warum glaubst du, dass die Technologie in Richtung Cloud Computing geht? Begründe deine Ideen anhand dessen, was du zuvor über Cloud Computing gelernt hast.``

>Ein weiterer Dienst, der in die Cloud verlagert werden könnte, ist die Bild- und Videobearbeitung. Cloud-basierte Anwendungen bieten Zugriff auf leistungsstarke Tools von verschiedenen Geräten aus. Die Technologie geht in Richtung Cloud Computing, da sie Zugänglichkeit, Kosteneffizienz, Skalierbarkeit, Zusammenarbeit und einfache Wartung bietet. Dies treibt die Verschiebung von lokalen Anwendungen in die Cloud voran, um Benutzern ein flexibleres und effizienteres Erlebnis zu bieten.

- [Regions and Availability Zones](https://aws.amazon.com/about-aws/global-infrastructure/regions_az/)
- [AWS Global Infrastructure](https://aws.amazon.com/about-aws/global-infrastructure/)
- [AWS Global Cloud Infrastructure](https://aws.amazon.com/cloud-infrastructure/)

##### Übersicht

Du wirst etwas über die drei verschiedenen Arten von Cloud-Services lernen. Wir werden die Vorteile der Verwendung dieser Cloud-Services im Vergleich zu traditionellen Modellen besprechen.


**Zu berücksichtigende Faktoren:**

- AWS-Kosten
- Verfügbarkeit von Diensten
- Geschwindigkeit oder Latenz
- Robustheit der AWS-Komponenten
- Datenrechte
- Zielgruppe

