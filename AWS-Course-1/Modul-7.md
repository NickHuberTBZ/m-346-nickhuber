## Modul 1 - Course 1

### `Quellen: Zusammengefasst ChatGPT und Course 1`

[TOC]

### Zweck des Moduls:

In diesem Modul erhalten Sie einen Überblick über die Cloud-Sicherheit in Bezug auf die AWS Identity and Access Management (IAM). Dies umfasst Informationen zu bewährten Verfahren, Rollen, Benutzern, Richtlinien und Sicherheitsgruppen.

### Modulbeschreibung:

Die Aktivitäten in diesem Modul werden Sie dazu veranlassen, Notizen über präsentierte und gesammelte Informationen zu machen. Sie werden auch eine Diskussion oder ein Rollenspiel über die sozialen Auswirkungen der Cloud-Sicherheit führen. Sie werden den Prozess zur Behebung von Schwachstellen in einem Webserver bestimmen.

### Technische Terminologie:

Um mehr über die in diesem Modul verwendete technische Terminologie zu erfahren, erweitern Sie jede der folgenden zwölf Kategorien.

### AWS Identity and Access Management (IAM) 
Beinhaltet die Anwendung von Kontrollen für Benutzer, die Zugriff auf Rechenressourcen benötigen.

### Rolle 
Eine IAM-Identität, die Sie in Ihrem Konto erstellen können und die bestimmte Berechtigungen hat.

### Benutzer 
Eine Entität, die Sie in Amazon Web Services (AWS) erstellen, um die Person oder Anwendung zu repräsentieren, die sie zur Interaktion mit AWS verwendet. Ein Benutzer in AWS besteht aus einem Namen und Anmeldeinformationen.

### Sicherheitsgruppe
Eine Sicherheitsgruppe fungiert als virtuelle Firewall für Ihre Instanz, um den ein- und ausgehenden Datenverkehr zu kontrollieren.

### Richtlinie 
Ein Objekt in AWS, das, wenn es mit einer Identität oder einer Ressource verknüpft ist, deren Berechtigungen definiert. AWS wertet diese Richtlinien aus, wenn eine Hauptentität (Benutzer oder Rolle) eine Anfrage stellt.

### Amazon Inspector 
Hilft Kunden, Sicherheitslücken und Abweichungen von bewährten Sicherheitspraktiken in Anwendungen zu identifizieren, bevor sie bereitgestellt werden und während sie in einer Produktionsumgebung ausgeführt werden.

### Gruppe
Eine IAM-Gruppe ist eine Sammlung von IAM-Benutzern. Gruppen ermöglichen es Ihnen, Berechtigungen für mehrere Benutzer festzulegen, was die Verwaltung der Berechtigungen für diese Benutzer erleichtern kann.

### Stamm-Benutzer 
Wenn Sie ein AWS-Konto erstellen, beginnen Sie mit einer einzigen Anmeldeidentität, die vollen Zugriff auf alle AWS-Dienste und Ressourcen im Konto hat.

### Anmeldeinformatio 
AWS-Sicherheitsanmeldeinformationen überprüfen, wer Sie sind und ob Sie die Berechtigung haben, auf die angeforderten Ressourcen zuzugreifen.

### Aktivieren der Multi-Faktor-Authentifizierung (MFA)
Dieser Ansatz zur Authentifizierung erfordert zwei oder mehr unabhängige Informationen, um authentifiziert zu werden.

### JavaScript Object Notation (JSON)
Eine Syntax zum Speichern und Austauschen von Daten.

### Multi-Faktor-Authentifizierung (MFA)
Ein Sicherheitssystem, das mehr als eine Methode der Authentifizierung aus unabhängigen Kategorien von Anmeldeinformationen erfordert, um die Identität des Benutzers für einen Login oder eine andere Transaktion zu überprüfen.

### Hintergrund und Missverständnisse 

Sicherheit ist von entscheidender Bedeutung, wenn Sie Cloud-Ressourcen zur Verarbeitung und Speicherung von Daten verwenden. Da Datenbanken, Websites und Apps in der Cloud sensible Informationen wie Bank- und medizinische Aufzeichnungen verarbeiten können, müssen diese Cloud-Ressourcen einschränken, wer auf sie zugreifen kann und welche Berechtigungen sie dabei haben.

IAM beinhaltet die Anwendung von Kontrollen für Benutzer, die Zugriff auf Rechenressourcen benötigen. Das AWS-Konto eines Unternehmens kann von Dutzenden unterschiedlicher Personen verwaltet werden, die sich in verschiedenen Abteilungen oder Büros befinden, unterschiedliche Verantwortlichkeiten oder Senioritätsstufen haben und sogar in verschiedenen Ländern tätig sein können. Um eine sichere Cloud-Umgebung mit all diesen Variablen aufrechtzuerhalten, ist es entscheidend, bewährte Verfahren für IAM beizubehalten. Um die bewährten Verfahren zu überprüfen, siehe die Sicherheitsbewährten Verfahren in IAM im AWS Identity and Access Management Benutzerhandbuch.

### AWS-Identitäten

Wenn Sie über IAM in AWS nachdenken, gibt es Rollen, Identitäten und Gruppen, die alle von Richtlinien geregelt werden.

Auf der höchsten Ebene befindet sich der Stamm-Benutzer. Dies ist die Identität, die das AWS-Konto erstellt hat. Der Stamm-Benutzer hat Zugriff auf jeden Aspekt von AWS und fungiert als universeller Administrator. Die Anmeldeinformationen des Stamm-Benutzers sollten niemals weitergegeben werden, und es wird sogar nicht empfohlen, dass der Kontoersteller alltägliche Aufgaben als Stamm-Benutzer durchführt. Stattdessen sollte das Konto des Stamm-Benutzers verwendet werden, um ein Administrator-Konto zu erstellen. Nur wenige Aufgaben müssen als Stamm-Benutzer erledigt werden, wie z.B. die Änderung des AWS-Supportplans oder die Kontoschließung.

Ein IAM-Benutzer ist eine Entität, die in AWS erstellt wird. Er repräsentiert die Person, die AWS-Dienste verwendet, und gibt den Personen die Möglichkeit, sich bei AWS anzumelden. Ein Benutzer wird einem Namen und einem Passwort zugewiesen, um auf die AWS-Konsole zuzugreifen. Bei der Erstellung eines Benutzers gilt es als bewährte Praxis, ihn einer Gruppe zuzuweisen, die die entsprechende Berechtigungsrichtlinie hat.

Eine Gruppe ist eine Sammlung von IAM-Benutzern. Sie können Gruppen verwenden, um Berechtigungen für eine Gruppe von Benutzern festzulegen, was die Verwaltung dieser Berechtigungen für diese Benutzer erleichtern kann. Sie könnten beispielsweise eine Gruppe namens Admins haben und dieser Gruppe die Arten von Berechtigungen geben, die Administratoren typischerweise benötigen. Jeder Benutzer in dieser Gruppe hat automatisch die Berechtigungen, die der Gruppe zugewiesen sind. Wenn ein ne
