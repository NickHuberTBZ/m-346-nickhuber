# Modul 8 - Course 1

## `Quellen: Zusammengefasst ChatGPT und Course 1`

[TOC]

---

## Zweck

Der Zweck dieses Moduls besteht darin, Ihr Verständnis für die Cloud-Sicherheit weiterzuentwickeln. Das Modul behandelt die Unterschiede zwischen AWS Shield und AWS WAF. Sie werden erfahren, was ein Distributed Denial of Service (DDoS)-Angriff ist. Außerdem werden Sie etwas über Amazon Inspector und AWS Artifact erfahren.

## Modulbeschreibung

In diesem Modul erhalten Sie Szenarien im Zusammenhang mit der AWS Cloud. Sie werden auch feststellen, ob bewährte Sicherheitspraktiken befolgt werden, und Schritte zur Behebung von Sicherheitslücken empfehlen.

## Technologische Terminologie

Um mehr über die in diesem Modul verwendete technologische Terminologie zu erfahren, erweitern Sie jede der folgenden fünf Kategorien.

### AWS Shield

AWS Shield ist ein Dienst, der Angriffe auf die Infrastruktur abwehrt und mindert, wobei der Schwerpunkt auf dem Netzwerk liegt, das zum Zugriff auf Cloud-Ressourcen verwendet wird.

### AWS WAF

AWS WAF (Web Application Firewall) ist ein Abwehrwerkzeug von AWS, das Webanwendungen vor Exploits schützt, die deren Verfügbarkeit, Sicherheit oder Ressourcenverbrauch beeinträchtigen könnten.

### Distributed Denial of Service (DDoS)

DDoS-Angriffe umfassen Angreifer, die Programme einrichten, die gleichzeitig eine massive Menge von Anfragen an eine Anwendung, eine Website oder einen Dienst senden. Diese Traffic-Flut kann Ressourcen überlasten und den Dienst für legitime Benutzer unzugänglich machen.

### Amazon Inspector

Amazon Inspector ist ein Dienst, der die Sicherheit von AWS-Ressourcen, wie EC2-Instanzen, überprüft, indem er nach Schwachstellen sucht und die Einhaltung bewährter Praktiken sicherstellt.

### AWS Artifact

AWS Artifact ist eine zentrale Ressource für informationen im Zusammenhang mit Compliance. Sie bietet Details zu den verschiedenen Compliance-Standards und Zertifizierungen, die von AWS erfüllt werden, was für Organisationen, die sensible Daten verarbeiten, von entscheidender Bedeutung ist.

## Hintergrund und Missverständnisse

Die Sicherheit in der Cloud umfasst vier wesentliche Bereiche:

1. **Daten**: Schutz der in der Cloud gespeicherten und verarbeiteten Daten.
2. **Berechtigungen**: Regulierung des Zugriffs auf Cloud-Ressourcen und Daten.
3. **Infrastruktur**: Schutz der Hardware und Maschinen, auf denen Daten in der Cloud ausgeführt, gespeichert und verarbeitet werden.
4. **Bewertung**: Überprüfung der Infrastruktur, Berechtigungen und Daten, um sicherzustellen, dass sie sicher sind.

In diesem Modul liegt der Schwerpunkt auf Infrastruktur und Bewertung.

**AWS Shield und AWS WAF** sind Dienste, die darauf ausgelegt sind, die Sicherheit der Infrastruktur zu gewährleisten, insbesondere im Hinblick auf das Netzwerk, das zum Zugriff auf Cloud-Ressourcen verwendet wird. Sie helfen, Angriffe abzuwehren.

**Amazon Inspector** behandelt die Bewertung, indem er untersucht, wie gut Cloud-Ressourcen, wie EC2-Instanzen, geschützt sind und ob bewährte Praktiken eingehalten werden.

Die Art der Cloud-Computing macht sie anfällig für Cyberangriffe, die Websites, Anwendungen und Prozesse beeinträchtigen können. Insbesondere DDoS-Angriffe stellen eine erhebliche Bedrohung dar. Diese Angriffe beinhalten das Überwältigen eines Ziels mit einer massiven Menge von Datenverkehr gleichzeitig.

**AWS Shield** arbeitet in Verbindung mit Elastic Load Balancing, Amazon CloudFront und Amazon Route 53, um sich gegen DDoS-Angriffe zu verteidigen. Es bietet zwei Serviceebenen:

- **AWS Shield Standard**: Dies steht allen AWS-Benutzern ohne zusätzliche Kosten zur Verfügung und bietet Schutz vor häufig auftretenden DDoS-Angriffen. Es wird automatisch auf Elastic Load Balancing-Ressourcen, CloudFront-Verteilungen und Route 53-Ressourcen angewendet.

- **AWS Shield Advanced**: Diese Ebene bietet erweiterte DDoS-Mitigationsfähigkeiten für volumetrische Angriffe, intelligente Angriffserkennung und Schutz vor Angriffen auf Anwendungs- und Netzwerkebene. Sie enthält rund um die Uhr Zugang zum DDoS-Response-Team (DRT) für individuelle Abwehrmaßnahmen, Echtzeitmetriken und Berichte sowie Schutz vor DDoS-Kosten. Shield Advanced ist kostenpflichtig.

**AWS WAF (Web Application Firewall)** ist ein weiteres Abwehrwerkzeug von AWS. Es schützt Webanwendungen vor Exploits, die die Verfügbarkeit, Sicherheit oder den Ressourcenverbrauch beeinträchtigen könnten. AWS WAF kann den Webdatenverkehr überwachen und auf der Grundlage von spezifischen Regeln, die von AWS-Benutzern erstellt wurden, entscheiden, welcher Datenverkehr zugelassen wird.

**Amazon Inspector** schützt AWS-Dienste nicht aktiv, überwacht sie jedoch auf Schwachstellen und Abweichungen von bewährten Praktiken. Es hilft sicherzustellen, dass Sicherheits-Compliance-Standards eingehalten werden und ist sowohl für Sicherheitsexperten als auch für Neueinsteiger wertvoll.

**AWS Artifact** dient als zentrale Ressource für informationen im Zusammenhang mit Compliance. Organisationen, die sensible Daten verarbeiten, müssen sicherstellen, dass ihr Cloud-Dienst bestimmten Sicherheitsstandards entspricht, und AWS Artifact bietet Details zu den Zertifizierungen und Regeln, die von AWS erfüllt werden.
