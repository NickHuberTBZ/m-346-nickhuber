## Modul 3 - Course 1

 `Quellen: Zusammengefasst ChatGPT und Course 1`

[TOC]

---

#### Zweck

In diesem Modul lernst du, wie du auf einige der gängigsten Dienste von Amazon Web Services (AWS) in der Konsole zugreifen und zu ihnen navigieren kannst. Du wirst auch mehr über die praktischen Anwendungen dieser Dienste in der realen Welt erfahren.

#### Modulbeschreibung

Dieses Modul beinhaltet eine von Lehrern geleitete Aktivität, die dich mit den Kernservices von AWS vertraut macht. Anschließend wirst du in der AWS-Konsole arbeiten, um das Navigieren zu verschiedenen Diensten zu üben. Schließlich wirst du eine Forschungsaktivität abschließen, um herauszufinden, wie diese Dienste in verschiedenen Branchen verwendet werden.

#### Technologische Terminologie

Erweitere die folgenden elf Kategorien, um die verwendete technologische Terminologie besser zu verstehen.

- **Amazon Simple Storage Service (Amazon S3)**: Ein AWS-Dienst, der Daten in der Cloud für Benutzer speichert.
- **Amazon Elastic Compute Cloud (Amazon EC2)**: Ein Webdienst, der sichere und skalierbare Rechenkapazität in der Cloud bereitstellt - vergleichbar mit der Anmietung eines Computers in der Cloud.
- **Amazon Elastic Block Store (Amazon EBS)**: Speicher für bestimmte EC2-Instanzen - vergleichbar mit der Festplatte für deine EC2-Instanz.
- **Amazon Relational Database Service (Amazon RDS)**: Ermöglicht Entwicklern das Erstellen und Verwalten relationaler Datenbanken in der Cloud. Diese verwenden SQL für Datenabfragen.
- **Amazon DynamoDB**: Ein nichtrelationales Datenbank-Service, bei dem Daten in Schlüssel-Wert-Paaren gespeichert werden.
- **AWS Lambda**: Ermöglicht das Ausführen von Code ohne die Verwaltung von Servern. Du zahlst nur für die verbrauchte Rechenzeit.
- **Amazon Virtual Private Cloud (Amazon VPC)**: Bietet ein dediziertes virtuelles Netzwerk für dein AWS-Konto - isoliert von anderen virtuellen Netzwerken.
- **AWS Identity and Access Management (IAM)**: Regelt den Zugriff auf AWS-Ressourcen für Benutzer.
- **AWS CloudTrail**: Überwacht Aktionen in deinem AWS-Konto zu Sicherheitszwecken.
- **Amazon CloudWatch**: Ein Überwachungsdienst für AWS-Ressourcen und Anwendungen.
- **Amazon Redshift**: Ein Datenbank-Service für die schnelle Abfrage großer Datenmengen.

#### Hintergrund und Missverständnisse

Dieses Modul führt in viele neue AWS-Dienste und Ressourcen ein. Die folgenden Websites bieten eine gute Einführung in diese Themen:

- [Amazon VPC Übersicht](https://aws.amazon.com/vpc/)
- [Lambda Übersicht](https://aws.amazon.com/lambda/)
- [Amazon EC2 Übersicht](https://aws.amazon.com/ec2/)
- [Amazon S3 Übersicht](https://aws.amazon.com/s3/)
- [Amazon EBS Übersicht](https://aws.amazon.com/ebs/)
- [Amazon RDS Übersicht](https://aws.amazon.com/rds/)
- [DynamoDB Übersicht](https://aws.amazon.com/dynamodb/)
- [Amazon Redshift Übersicht](https://aws.amazon.com/redshift/)
- [CloudWatch Übersicht](https://aws.amazon.com/cloudwatch/)
- [CloudTrail Übersicht](https://aws.amazon.com/cloudtrail/)

AWS-Cloud-Services umfassen eine Vielzahl von Tools, die zusammenarbeiten, um die Rechenbedürfnisse in der Cloud abzudecken.

Amazon VPC ist das virtuelle Netzwerk, das du definierst, wenn du AWS-Ressourcen startest. Dieses virtuelle Netzwerk ähnelt einem traditionellen Netzwerk in einem eigenen Rechenzentrum, bietet jedoch die Skalierbarkeit von AWS.

Hier sind einige Unterschiede zwischen den Diensten:

- Amazon S3 und Amazon EBS sind beide Datenspeicherungen. Einige Schlüsselunterschiede sind:
  - Amazon EBS wird nur an EC2-Instanzen angehängt, während auf Amazon S3 direkt zugegriffen werden kann.
  - Amazon EBS kann nicht so viel speichern wie Amazon S3.
  - Amazon EBS kann nur an eine EC2-Instanz angehängt werden, während Daten in einem S3-Bucket von mehreren EC2-Instanzen abgerufen werden können.
  - Amazon S3 hat längere Verzögerungen beim Schreiben von Daten als Amazon EBS.
- Amazon RDS, Amazon Redshift und DynamoDB sind alle mit Datenbanken verbunden, aber es gibt Unterschiede:
  - Amazon RDS ist die klassische relationale Datenbank mit SQL Server, Oracle Database, Amazon Aurora oder ähnlichen Systemen. Dies ist vergleichbar mit einem Notenbuch, bei dem jeder Schüler eine Zeile ist und die gleiche Anzahl von Aufgaben (Spalten) hat, die ihnen zugeordnet sind.
  - Amazon Redshift ist eine relationale Datenbank für große Datenmengen.
  - DynamoDB ist eine nichtrelationale Datenbank mit flexibler Speicherung von Schlüssel-Wert-Paaren.

CloudTrail und CloudWatch sind Überwachungsdienste, aber sie haben unterschiedliche Funktionen:

- CloudTrail überwacht Aktionen in einem AWS-Konto für Sicherheitszwecke.
- CloudWatch überwacht Ressourcennutzung und Anwendungen in der Cloud.
