## Modul 1 - Course 1

### `Quellen: Zusammengefasst ChatGPT und Course 1`

[TOC]

---

#### Zweck
Der Zweck dieses Moduls besteht darin, die Grundlagen des Cloud-Computings zu überprüfen. Es werden die Vorteile der Bereitstellung von IT-Ressourcen in der Cloud sowie die Gründe, warum Unternehmen begonnen haben, von lokalem Computing zu Cloud Computing zu wechseln, behandelt. Es wird auch auf die Hauptdienste eingegangen, die von Cloud-Computing-Anbietern angeboten werden.

#### Modulbeschreibung
In diesem Modul werden Sie Notizen zu den Vorteilen des Cloud-Computings und den von Cloud-Anbietern angebotenen Diensten machen. Wir werden auch einige Auswirkungen des Cloud-Computings besprechen.

#### Technologiebegriffe
Um mehr über die in diesem Modul verwendeten Technologiebegriffe zu erfahren, erweitern Sie jede der folgenden vier Kategorien.

---

>##### Cloud Computing
>- Die bedarfsgerechte Bereitstellung von Rechenleistung, Datenbanken, Speicher, Anwendungen und anderen IT-Ressourcen über das Internet >mit Pay-as-you-go-Preisen.
>
>##### Amazon Web Services (AWS)
>- Eine Plattform, die eine breite Palette von Cloud-Computing-Diensten bietet.
>
>##### Cloud-Speicher
>- Speichern von Daten mithilfe eines Cloud-Service-Providers (CSP) anstelle einer physischen Maschine.
>
>##### Server
>- Ein Computer, der dazu entwickelt wurde, Anfragen zu verarbeiten und Daten an einen anderen Computer über das Internet oder ein lokales Netzwerk zu liefern. In der Cloud wird ein Server von einem externen Anbieter gehostet, der über das Internet zugänglich ist.

#### Hintergrund und Missverständnisse

##### Was ist Cloud Computing?
Jedes Mal, wenn Sie online arbeiten oder Informationen speichern (zum Beispiel eine E-Mail senden oder ein Streaming-Video ansehen) – im Gegensatz zur lokalen Speicherung auf Ihrem Computer oder einem Server in Ihrem lokalen Netzwerk – verwenden Sie Cloud Computing.

##### Warum nutzen Unternehmen Cloud Computing?
Die geschäftlichen Vorteile des Cloud-Computings umfassen Folgendes:

- Geringere Anfangskosten für Ihr Unternehmen. Bei Wachstum höhere Kosten.
- Dienste sind kostengünstiger, da die Kosten auf viele Benutzer verteilt werden.
- Ihre Rechenleistung und Speicher skaliert entsprechend Ihrem Bedarf, sodass Sie nur für das bezahlen, was Sie nutzen.
- Das Hinzufügen neuer Ressourcen, wenn Sie sie benötigen, ist schneller und einfacher.
- Cloud-Anbieter warten, sichern und betreiben die Computer und Einrichtungen für Cloud-Dienste.
- Es ist einfach, Ihre Anwendung online zu veröffentlichen oder überall zu bewerben, da alles online verfügbar ist.

#### Welche Arten von Cloud-Diensten gibt es?

| Art des Cloud-Dienstes | Was er macht | Beispiele |
|------------------------|--------------|-----------|
| Infrastructure as a Service (IaaS) | Rechenleistung, Netzwerken und Speicherung über das Internet bereitstellen | Amazon Elastic Compute Cloud (Amazon EC2), Rackspace, Google Compute Engine |
| Platform as a Service (PaaS) | Werkzeuge über das Internet bereitstellen, um Programme und Anwendungen zu erstellen | AWS Elastic Beanstalk, Microsoft Azure, Google App Engine |
| Software as a Service (SaaS) | Über das Internet zugängliche Anwendungen und Programme | Dropbox, Slack, Spotify, YouTube, Microsoft Office 365, Gmail |

#### Wie hat AWS begonnen?

- Die Ursprünge begannen 2002, als Amazon den Amazon.com-Webdienst startete.
- Bereitgestellte Tools für Entwickler, um am Amazon-Produktkatalog zu arbeiten
- Im Jahr 2003 erkannte Amazon, dass seine Infrastrukturdienste ihnen einen Vorteil gegenüber der Konkurrenz verschaffen könnten.
- Stellte Hardwareleistung, Speicherung und Datenbanken sowie die Softwaretools zur Steuerung bereit
- Im Jahr 2004 kündigte Amazon öffentlich an, an einem Cloud-Dienst zu arbeiten.
- Im Jahr 2006 startete Amazon AWS mit nur wenigen der heutzutage noch verfügbaren Dienste.
- Amazon Simple Storage Service (Amazon S3)
- Amazon EC2 
- Amazon Simple Queue Service (Amazon SQS)
- Bis 2009 fügte AWS weitere Dienste hinzu
- Amazon Elastic Block Store (Amazon EBS)
- Amazon CloudFront – ein Content Delivery Network (CDN)
- AWS hat Partnerschaften mit mehreren großen Unternehmen entwickelt. AWS ist seitdem stetig gewachsen und hat kontinuierlich neue Dienste und Tools hinzugefügt.

#### Leitfragen

##### Fragen

`1. Stellen Sie sich vor, eines Ihrer Social-Media-Konten würde gehackt und alle Ihre Daten würden öffentlich gemacht oder gegen Lösegeld zurückgehalten. Wie würde sich das für Sie anfühlen? Glauben Sie, dass der Kompromiss das Risiko wert ist, um alle Cloud-Dienste zur Verfügung zu haben?`
>
>Ein gehacktes Social-Media-Konto mit öffentlich gemachten oder erpressten Daten wäre äußerst besorgniserregend. Ob der Nutzen von Cloud-Diensten das Risiko wert ist, hängt von deren Sicherheit ab.
>
`2. Welche Art von Informationen haben Sie online gespeichert? Welche Risiken bestehen, dass diese Informationen kompromittiert oder ohne Ihre Zustimmung geteilt werden? Welche Arten von Gesetzen oder Vorschriften halten Sie für notwendig, um Ihre Informationen sicher zu halten?`
>
>Online habe ich persönliche Fotos, E-Mails und soziale Interaktionen gespeichert. Risiken sind Diebstahl, Missbrauch und unerlaubte Weitergabe. Strengere Datenschutzgesetze und -maßnahmen sind nötig.
>
`3. Auf welche Weise hat das Internet Ihr Leben erleichtert? Auf welche Weise hat das Internet Ihr Leben schwieriger gemacht? Was ist etwas, das Sie gerne online tun würden, für das die Technologie noch nicht existiert?`
>
>Das Internet erleichtert den Zugang zu Informationen, Kommunikation und Einkaufsmöglichkeiten. Gleichzeitig bringt es Datenschutzbedenken und Ablenkungen mit sich. Ich wünsche mir eine sichere Möglichkeit zur digitalen Unterschrift von Dokumenten.
>
#### Aktivität 1: Einführung in das Cloud Computing

##### Übersicht
In dieser Aktivität arbeiten Sie in Paaren, um Notizen über die Grundlagen des Cloud-Computings zu machen. Sie werden Quellen finden, um Definitionen für Schlüsselbegriffe zu finden und die Vorteile aufzulisten, die das Cloud-Computing bietet. Außerdem werden Sie die wichtigsten Dienste identifizieren, die von Cloud-Computing-Anbietern angeboten werden, und Beispiele dafür geben, wie diese Dienste in der Industrie verwendet werden.

##### Ziele
Cloud Computing und seine Auswirkungen:

**Cloud Computing** bezeichnet die Bereitstellung von IT-Ressourcen über das Internet. Unternehmen und Einzelpersonen können Rechenleistung, Speicher und Anwendungen nach Bedarf nutzen, ohne physische Hardware besitzen zu müssen. Die Auswirkungen sind:

- **Flexibilität:** Schnelle Anpassung der Ressourcen an wechselnde Anforderungen.
- **Kostenersparnis:** Reduzierte Ausgaben für Hardware und Wartung.
- **Effizienz:** Effektive Ressourcennutzung und -verwaltung.
- **Globaler Zugriff:** Daten und Anwendungen von überall aus erreichbar.
- **Schnelle Bereitstellung:** Rasches Einrichten neuer Ressourcen und Anwendungen.

**Vorteile des Cloud Computing:**

- **Skalierbarkeit:** Anpassung von Ressourcen nach Bedarf.
- **Kosteneffizienz:** Bezahlen nur für genutzte Ressourcen.
- **Innovation:** Zugriff auf neue Technologien und Services.
- **Weltweiter Zugang:** Datenzugriff und Zusammenarbeit von überall.
- **Entlastung der IT:** Externe Wartung und Management.
- **Notfallwiederherstellung:** Schutz vor Datenverlust und Ausfällen.

Insgesamt hat Cloud Computing die IT-Nutzung und -Verwaltung revolutioniert und bringt vielfältige Vorteile mit sich.


