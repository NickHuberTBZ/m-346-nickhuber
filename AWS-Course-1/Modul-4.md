## Modul 4 - Course 1

### `Quellen: Zusammengefasst ChatGPT und Course 1`

[TOC]

---
### Zweck

In diesem Modul lernst du, wie du eine Amazon Elastic Compute Cloud (Amazon EC2)-Instanz erstellst und sie zur Hosting einer Website nutzt. Du wirst auch den Zweck von Zugriffsschlüsseln, dem Domain Name System (DNS), Amazon Route 53 und Virtuellen Privaten Clouds kennenlernen.

### Modulbeschreibung

Die Aktivitäten in diesem Modul umfassen praktische Arbeiten, darunter das Erstellen einer EC2-Instanz, das Anfügen eines Zugriffsschlüssels, das Zugreifen auf die Instanz über die Befehlskonsole sowie das Erstellen einer einfachen Website und deren Hosting auf der EC2-Instanz mithilfe eines Amazon Simple Storage Service (Amazon S3)-Buckets.

### Technologische Begrifflichkeiten

Um mehr über die in diesem Modul verwendeten technologischen Begriffe zu erfahren, erweitere jede der folgenden elf Kategorien.

- **Amazon Elastic Compute Cloud (Amazon EC2)**:
Ein Webdienst, der sichere, skalierbare Rechenkapazität in der Cloud bereitstellt. Stelle es dir vor wie das Mieten eines Computers in der Cloud.

- **Amazon Simple Storage Service (Amazon S3)**:
Ein von Amazon Web Services (AWS) bereitgestellter Dienst, der Daten für Benutzer in der Cloud speichert.

- **Domain Name System (DNS)**:
Ein Namenssystem für Computer, Geräte und Ressourcen, die mit einem Netzwerk verbunden sind.

- **S3-Bucket**:
Ein Container für Objekte (wie Bilder, Audiodateien, Videodateien, Dokumente usw.) in Amazon S3.

- **Richtlinie (Policy)**:
Ein Objekt in AWS, das, wenn es mit einer Identität oder einer Ressource verknüpft ist, deren Berechtigungen definiert. AWS wertet diese Richtlinien aus, wenn eine Hauptentität (Benutzer oder Rolle) eine Anfrage stellt.

- **Domänenname**:
Ein Label, das ein Netzwerk von Computern unter zentralisierter Kontrolle identifiziert.

- **Amazon Route 53**:
Der AWS DNS-Webdienst.

- **Virtuelles privates Cloud (VPC)**:
Ein virtuelles Netzwerk, das deinem AWS-Konto gewidmet ist. Es ist logisch von anderen virtuellen Netzwerken in der AWS Cloud isoliert. Alle deine AWS-Dienste können aus einem VPC gestartet werden. Es ist nützlich, um deine Daten zu schützen und zu verwalten, wer auf dein Netzwerk zugreifen kann.

- **JavaScript Object Notation (JSON)**:
Eine Syntax zum Speichern und Austauschen von Daten.

- **Dynamische Website**:
Eine Website, die sich basierend auf Benutzerinteraktionen ändert; oft erstellt mit Python, JavaScript, PHP oder ASP in Kombination mit Hypertext Markup Language (HTML).

- **Statische Website**:
Eine Website, die sich basierend auf Benutzerinteraktionen nicht ändert; typischerweise erstellt mit HTML und Cascading Style Sheets (CSS).

### Hintergrund und Missverständnisse

Amazon EC2 ist einer der grundlegendsten und am häufigsten genutzten AWS-Cloud-Dienste. Grundsätzlich bietet er Benutzern Rechenleistung in der Cloud, die für Aufgaben genutzt werden kann: von maschinellem Lernen über das Ausführen von Anwendungen bis hin zum Abfragen von Datenbanken und Streamen von Videos.

Es gibt zwei Arten von Websites: statische und dynamische. Statische Websites erfordern keine Aktionen auf dem Server. Dynamische Websites erfordern Interaktionen mit dem Server, um auf Anfragen auf den Client-Maschinen zu reagieren. In diesem Modul musst du einen S3-Bucket erstellen, der wie ein Ordner ist, um deine statische Website zu speichern. Du wirst deinen Bucket öffentlich machen, damit jeder den Inhalt sehen kann. Dies ergibt Sinn für eine öffentliche Website, aber du solltest die Datenschutzkontrollen für verschiedene Arten von Daten berücksichtigen. Wenn du beispielsweise ein privates Tagebuch in der Cloud speicherst, möchtest du keinen öffentlichen Zugriff gewähren.

Das Objekt, das die Sicherheits- und Zugriffsberechtigungen in AWS steuert, wird Richtlinie (Policy) genannt. Richtlinien werden in einer Programmiersprache namens JSON geschrieben. Du musst für dieses Modul nicht wissen, wie man JSON schreibt; du wirst den Code einfach kopieren und einfügen. Du solltest wissen, dass JSON die Programmiersprache ist, die für AWS-Richtlinien verwendet
