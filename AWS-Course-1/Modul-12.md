# Modul 12 - Course 1

## `Quellen: Zusammengefasst ChatGPT und Course 1`

[TOC]

---

## Zweck

In diesem Modul lernen Sie den Zweck von Amazon ElastiCache und die Vorteile des Zwischenspeicherns von Daten kennen. Außerdem werden Sie etwas über Elastic Load Balancing erfahren.

## Modulbeschreibung

Sie werden Ihr Wissen über ElastiCache und ELB nutzen, um eine Anzeige für diese Dienste zu erstellen. Sie werden die AWS Management Console verwenden, um einen Lastenausgleich für eine Website zu erstellen und zu konfigurieren.

## Technologische Terminologie

Um mehr über die technologische Terminologie in diesem Modul zu erfahren, erweitern Sie jede der folgenden fünf Kategorien.

### Amazon ElastiCache

Ein Webdienst, der es einfach macht, einen In-Memory-Cache in der Cloud bereitzustellen, zu betreiben und zu skalieren. Der Dienst verbessert die Leistung von Webanwendungen, indem er es ermöglicht, Informationen aus schnellen, verwalteten, In-Memory-Caches abzurufen, anstatt auf langsamere datenträgerbasierte Datenbanken angewiesen zu sein.

### Cache

In der Informatik ist ein Cache eine Hochgeschwindigkeitsspeicherschicht, die eine Teilmengen von Daten speichert, die in der Regel von vorübergehender Natur sind, sodass zukünftige Anfragen nach diesen Daten schneller bedient werden können als durch den Zugriff auf den primären Speicherort der Daten.

### Datenzwischenspeicherung

Das Speichern von Daten in einem Cache ermöglicht es, zuvor abgerufene oder berechnete Daten effizient wiederzuverwenden. Die Daten in einem Cache werden in der Regel in schnellem Hardware-Speicher wie Random Access Memory (RAM) gespeichert und können auch mit einer Softwarekomponente verwendet werden.

### Elastic Load Balancing

Elastic Load Balancing verteilt eingehenden Anwendungsverkehr automatisch auf mehrere Ziele, wie Amazon Elastic Compute Cloud (Amazon EC2)-Instanzen, Container, IP-Adressen und AWS Lambda-Funktionen. Wenn der Verkehr zu einer Website plötzlich ansteigt, kann dieser Verkehr zu anderen im Voraus für diesen Zweck eingerichteten EC2-Instanzen (oder anderen Arten von Instanzen wie Lambda-Instanzen) umgeleitet werden. Diese Lastenausgleich vermeidet, dass ein einzelner Server aufgrund eines erhöhten Verkehrs überlastet wird.

### Random Access Memory (RAM)

Volatiler, temporärer Speicher. Dies sind die Daten, die vorübergehend gehalten werden, während eine Maschine in Betrieb ist. Sobald die Maschine ausgeschaltet ist oder die Aufgabe abgeschlossen ist, werden diese Daten gelöscht. Virtueller Speicher wird im ReadOnly-Speicher (ROM) als Ergänzung zum RAM gespeichert, wenn nicht genügend temporärer Speicherplatz vorhanden ist.

## Hintergrund und Missverständnisse

Von den vielen Möglichkeiten, Daten auf einem Computer zu verarbeiten, ist eine der häufigsten die Verarbeitung von schreibgeschützten Daten, die schnell präsentiert werden müssen und von einer großen Anzahl von Benutzern abgerufen werden, wie beispielsweise Musik oder Videos, die weltweit gestreamt werden. Diese Art von Daten wird selten aktualisiert oder gelöscht, aber es gibt eine große Menge davon, und die Nachfrage nach ihr kann dramatisch schwanken (denken Sie an ein Video oder einen Song, der viral geht). Da der Bedarf an diesem Typ von Zugriff so beliebt wird, bietet Amazon Web Services (AWS) Tools zur Bewältigung an. Die Tools sind hauptsächlich solche, die Daten schnell abrufen und Daten in Reaktion auf Spitzen und Täler der Nachfrage über mehrere Server verteilen können - und dies auf kosteneffektive Weise tun, die nur für die Nutzung berechnet.

Anwendungen und Websites bieten Benutzern oft eine breite Palette von Daten und Diensten an. Innerhalb dieser breiten Palette von Daten gibt es oft eine kleinere Teilmenge von Daten, die häufiger angefordert und abgerufen werden. Dies könnten die Daten auf der Startseite sein, die jedem Besucher angezeigt werden (denken Sie an die Top 10 Produkte des Tages bei Amazon) oder es könnte sich um ein kürzlich veröffentlichtes Medium handeln, das einen Popularitätsschub erlebt (ein neuer Song bei Spotify).

Andere Anwendungen führen Prozesse aus, die extrem speicherintensiv sind und unter Leistungsproblemen bei einem langsameren Speicherlaufwerk leiden könnten.

Für diese Art von stark angeforderten oder speicherintensiven Daten kann ein Daten-Caching-Dienst wie ElastiCache dazu beitragen, sicherzustellen, dass die Daten extrem schnell abgerufen und verarbeitet werden können. Dies geschieht durch Speichern der Daten in extrem schnellem, aber vorübergehendem Speicher, der schneller ist als speicherbasierte Datenspeicherung. Der Kompromiss besteht darin, dass der schnelle Speicher weniger Speicherplatz hat und die Daten nicht dauerhaft speichert.

Viele Unternehmen nutzen ElastiCache, um Echtzeit-Apps zu erstellen, den E-Commerce zu beschleunigen und ihre Websites zu zwischenspeichern.

Schwere Traffic-Spitzen können Apps und Websites zum Stillstand bringen, wenn der Server die Last nicht bewältigen kann. Daher hat AWS ELB, das erkennen kann, wenn es zu viele Anfragen gibt, und den Verkehr automatisch auf einen neuen Server umleiten kann, um Geschwindigkeit und Stabilität aufrechtzuerhalten. Es gibt drei Arten von ELB in AWS.

- **Anwendungslastenausgleich**: Der Anwendungslastenausgleich eignet sich am besten für die Lastenverteilung von Hypertext Transfer Protocol (HTTP)- und Secure HTTP (HTTPS)-Verkehr und bietet eine fortschrittliche Anfragerouting, das auf die Bereitstellung moderner Anwendungsarchitekturen abzielt, einschließlich Microservices und Container. Der Anwendungslastenausgleich arbeitet auf Ebene 7 (individuelle Anfrageebene) und leitet den Verkehr zu Zielen innerhalb von Amazon Virtual Private Cloud (Amazon VPC) basierend auf dem Inhalt der Anfrage.

- **Netzwerkausgleich**: Der Netzwerkausgleich ist am besten für die Lastenverteilung von Transmission Control Protocol (TCP), User Datagram Protocol (UDP

) und Transport Layer Security (TLS)-Verkehr geeignet, bei dem extreme Leistung erforderlich ist. Der Netzwerkausgleich arbeitet auf Verbindungsebene (Ebene 4) und leitet den Verkehr zu Zielen innerhalb von Amazon VPC und ist in der Lage, Millionen von Anfragen pro Sekunde zu verarbeiten und gleichzeitig extrem niedrige Latenzzeiten aufrechtzuerhalten. Der Netzwerkausgleich ist auch optimiert, um plötzliche und volatile Verkehrsverläufe zu bewältigen.

- **Klassischer Lastenausgleich**: Der klassische Lastenausgleich bietet eine grundlegende Lastenverteilung über mehrere EC2-Instanzen und arbeitet auf Anfrage- und Verbindungsebene. Der klassische Lastenausgleich ist für Anwendungen gedacht, die im EC2-Classic-Netzwerk erstellt wurden.
