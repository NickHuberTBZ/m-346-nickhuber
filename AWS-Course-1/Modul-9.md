## Modul 9 - Course 1

## `Quellen: Zusammengefasst ChatGPT und Course 1`

[TOC]

---

## Zweck

Der Zweck dieses Moduls besteht darin, Sie über die Tools zu informieren, die Amazon Web Services (AWS) für die Überwachung von Cloud-Diensten bereitstellt. Hierzu gehören AWS Config, AWS CloudTrail und Amazon CloudWatch.

## Modulbeschreibung

Beim Betrieb in der Cloud ist es wichtig, Aktivitäten zu verfolgen, da für jede Aktivität wahrscheinlich Kosten anfallen. AWS unterstützt die Überwachung, Protokollierung und Berichterstellung zur Nutzung seiner Dienste, indem es entsprechende Tools bereitstellt. In diesem Modul werden Sie diese Tools erkunden.

Im Labor werden Sie eine Benachrichtigung mit CloudWatch einrichten und CloudTrail-Protokolldateien überwachen. Sie werden auch die verschiedenen Verwendungszwecke von CloudTrail und CloudWatch besprechen. Schließlich werden Sie ermitteln, welche Dienste für ein bestimmtes Szenario am besten geeignet sind.

## Technologische Terminologie

Um mehr über die technologische Terminologie in diesem Modul zu erfahren, erweitern Sie jede der folgenden vier Kategorien.

### Amazon CloudWatch

- Ein Überwachungsdienst zur Überwachung Ihrer AWS-Ressourcen und der Anwendungen, die Sie auf AWS ausführen.

### AWS CloudTrail

- Ein Dienst zur Überwachung und Protokollierung jeder Aktion, die in Ihrem AWS-Konto aus Sicherheitsgründen durchgeführt wird.

### AWS Config

- Ein Dienst, der es Ihnen ermöglicht, die Konfigurationen Ihrer AWS-Ressourcen zu bewerten, zu überprüfen und zu bewerten.

### Amazon Simple Notification Service (Amazon SNS)

- Ein AWS-Tool, mit dem Sie Textnachrichten, E-Mails und Nachrichten an andere Cloud-Dienste senden und Benachrichtigungen in verschiedenen Formen von der Cloud an den Client senden können.

## Hintergrund und Missverständnisse

AWS bietet viele miteinander verbundene Dienste, die eine komplexe Grundlage bieten, um Aufgaben in der Cloud zu erledigen. Mit dem Wachstum von Unternehmen können mehrere miteinander verbundene AWS-Konten verwendet werden. Jedes Konto kann Dutzende von Instanzen ausführen, die Tausende von Gigabyte Daten verarbeiten, Millionen von Menschen bedienen und Milliarden von Dollar repräsentieren. Unabhängig von der Größe eines Unternehmens ist es unerlässlich, Ihre Cloud-Dienste zu überwachen und zu verfolgen. Dies hilft sicherzustellen, dass alle Cloud-Ressourcen reibungslos funktionieren und Sie darüber informiert sind, wenn etwas Ungewöhnliches geschieht.

AWS bietet leistungsstarke Tools zur Überwachung aller Cloud-Dienste. Diese Tools arbeiten zusammen, um eine Reihe von Diensten bereitzustellen, die Cloud-Benutzern Wissen vermitteln.

CloudWatch ist ein Überwachungsdienst zur Überwachung Ihrer AWS-Ressourcen und der Anwendungen, die Sie auf AWS ausführen.

CloudTrail und CloudWatch sind beide Cloud-Überwachungsdienste, erfüllen jedoch unterschiedliche Funktionen:

- CloudTrail überwacht und protokolliert alle Aktionen, die Benutzer in einem bestimmten AWS-Konto ausgeführt haben. Das bedeutet, dass CloudTrail jedes Mal protokolliert, wenn jemand Daten hochlädt, Code ausführt, eine Amazon Elastic Compute Cloud (Amazon EC2)-Instanz erstellt oder eine andere Aktion ausführt.

- CloudWatch überwacht, was alle verschiedenen Dienste tun und welche Ressourcen sie verwenden. CloudTrail protokolliert Aktivitäten, während CloudWatch Aktivitäten überwacht. CloudWatch hilft Ihnen sicherzustellen, dass Ihre Cloud-Dienste reibungslos laufen. Die Dienste können Ihnen auch helfen, nicht mehr oder weniger Ressourcen zu verwenden, als Sie erwarten, was für die Budgetverfolgung wichtig ist.

- AWS Config ist ein Dienst, mit dem Sie die Konfigurationen Ihrer AWS-Ressourcen bewerten, überprüfen und bewerten können. AWS Config überwacht kontinuierlich und protokolliert die Konfigurationen Ihrer AWS-Ressourcen und ermöglicht die Automatisierung der Auswertung von aufgezeichneten Konfigurationen gegenüber gewünschten Konfigurationen.
