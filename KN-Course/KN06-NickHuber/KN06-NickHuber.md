# KN06

### Reverse Proxy
Ein Reverse Proxy ist ein Server, der zwischen Client und Webserver steht. Er leitet Anfragen an den Webserver weiter, empfängt die Serverantworten und sendet sie zurück an die Clients. Reverse Proxies werden oft für Loadbalancing verwendet.
***

### Vertikale Skalierung
***
Ich bin einfach bei der gewünschten Instanz das Volumen erweitert ohne irgendwas zuvor geändert zu haben.
Ja man kann das Volumen im laufenden Betrieb ändern, denn das Volumen wird ergänzt und nicht ersetzt.

Man muss zuerst die Instanz runterfahren, dann kann man bei der gewünscten Instanz unter "Action" und dann "change instance type" den gewünschten Typ auswählen.    
Den Typ kann man nicht wärend dem Betrieb ändern, denn man ändert damit die basis der ganzen Maschine.

### Horizontale Skalierung
***
Es ist Sinnvoller eine satische IP zu haben, weil es die zuverlässigkeit verbesert. Weil man einen DNS-Alias haben kann, welche auf die IP-Adresse verweisen kann. Sicherheitsgruppen sind dafür sinnvoll, um zu kontrollieren wer zugriff auf unseren Loadbalancer hat.

### Erstellen Alias
***
- Anmeldem bei AWS-Managementkonsole und öffnen die Route 53-Konsole.
- Klicken auf Hosted Zone, in der der Alias-Eintrag erstellt wird.
- Klicken auf Alias-Einträge.
- Klicken auf Alias-Eintrag erstellen.
- Geben im Feld Name den DNS-Namen des Load Balancers ein.
- Wählen im Feld Typ die Option Load Balancer aus.
- Geben im Feld Load Balancer-Name den Namen des Load Balancers ein.
- Klicken auf Erstellen.

### Load Balancer
***
Ein Load Balancer ist ein Netzwerkgerät, das den Datenverkehr zwischen Clients und Servern verteilt. Load Balancer können dazu beitragen, die Leistung und Zuverlässigkeit von Anwendungen zu verbessern, indem sie den Traffic auf mehrere Server verteilen.
### Target Group
***
Eine Target Group ist eine Sammlung von Instances, die von einem Load Balancer verwendet werden. Load Balancer verteilen den Traffic auf Instances in einer Target Group.
### Health Check
***
Ein Health Check ist ein Prozess, der die Verfügbarkeit und Gesundheit von Instances in einer Target Group überprüft. Load Balancer verwenden Health Checks, um Instances aus einer Target Group zu entfernen, die nicht verfügbar oder nicht gesund sind.
### IPs
***
Eine IP-Adresse ist eine eindeutige Adresse, die einem Computer oder einem anderen Netzwerkgerät zugewiesen wird. AWS-Instances  erhalten eine IP-Adresse, wenn sie erstellt werden.
### Sicherheitsgruppen
***
Eine Sicherheitsgruppe ist eine Regelsammlung, die den Netzwerkverkehr für eine Instance oder einen Load Balancer regelt. Sicherheitsgruppen können dazu beitragen, die Sicherheit von Instances und Load Balancer zu verbessern, indem sie den Zugriff auf diese Ressourcen von bestimmten IP-Adressen oder Netzwerken einschränken.
### Lister
***
Ein Lister ist ein Tool, das die Instances in einer Target Group auflisten kann. Lister können hilfreich sein, um die Verfügbarkeit von Instances oder in einer Target Group zu überprüfen.
### Templates
***
Ein Template ist eine Vorlage, die verwendet werden kann, um AWS-Ressourcen zu erstellen. Templates können verwendet werden, um wiederholbare Aufgaben zu automatisieren und Fehler zu vermeiden.