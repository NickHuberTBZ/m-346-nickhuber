## Virtualisierung

**Virtualisierung** bezieht sich auf die Technologie, bei der eine einzige physische Hardware-Ressource in mehrere virtuelle Instanzen oder Umgebungen aufgeteilt wird. Dies ermöglicht es, mehrere Betriebssysteme oder Anwendungen auf derselben physischen Hardware auszuführen, ohne dass sie sich gegenseitig stören. Virtualisierung bietet Flexibilität, Skalierbarkeit und effiziente Ressourcennutzung.

### Hypervisor

Ein **Hypervisor** ist eine Software oder eine Hardware-Komponente, die die Virtualisierung verwaltet. Der Hypervisor erstellt und verwaltet virtuelle Maschinen (VMs), auf denen Betriebssysteme und Anwendungen ausgeführt werden. Der Hypervisor stellt sicher, dass die Ressourcen der physischen Hardware zwischen den VMs aufgeteilt werden, ohne Konflikte zu verursachen.

### Typ 1 vs. Typ 2 Hypervisors

Es gibt zwei Hauptarten von Hypervisoren:

- **Typ 1 Hypervisor**: Diese Hypervisoren werden auch als "bare-metal" Hypervisoren bezeichnet, da sie direkt auf der physischen Hardware laufen, ohne ein Host-Betriebssystem zu benötigen. Typ-1-Hypervisoren bieten eine höhere Leistung und Effizienz, da sie keine zusätzliche Schicht zwischen der Hardware und den VMs haben. Beispiele sind VMware vSphere/ESXi und Microsoft Hyper-V.

- **Typ 2 Hypervisor**: Diese Hypervisoren werden als "Hosted" Hypervisoren bezeichnet, da sie auf einem Host-Betriebssystem ausgeführt werden. Dies bedeutet, dass sie eine zusätzliche Abstraktionsebene haben und daher in der Regel weniger effizient sind als Typ-1-Hypervisoren. Typ-2-Hypervisoren sind nützlich für Entwicklung, Tests und Schulungszwecke. Ein Beispiel ist Oracle VirtualBox.

### Hyperscaler

Ein **Hyperscaler** bezieht sich auf große Cloud-Anbieter, die über umfangreiche Rechenzentren und Infrastruktur verfügen, um skalierbare und hochverfügbare Cloud-Dienste anzubieten. Diese Anbieter betreiben globale Netzwerke von Servern und Speichergeräten, um Kunden weltweit Cloud-Computing-Ressourcen bereitzustellen. Beispiele für Hyperscaler sind Amazon Web Services (AWS), Microsoft Azure und Google Cloud Platform (GCP).

### Schichten von Cloud-Systemen

Cloud-Systeme bestehen aus verschiedenen Schichten oder Ebenen:

1. **Infrastruktur as a Service (IaaS)**: Diese Schicht stellt grundlegende IT-Infrastrukturkomponenten wie virtuelle Maschinen, Speicher und Netzwerke bereit. Kunden können diese Ressourcen mieten und verwalten ihre Betriebssysteme und Anwendungen selbst.

2. **Platform as a Service (PaaS)**: In dieser Schicht wird die Infrastruktur weiter abstrahiert, und Kunden können Anwendungen entwickeln und bereitstellen, ohne sich um die zugrunde liegende Infrastruktur kümmern zu müssen. Das PaaS-Modell bietet Entwicklern eine Umgebung, um Anwendungen zu erstellen und zu hosten.

3. **Software as a Service (SaaS)**: Hierbei handelt es sich um vollständig gehostete Anwendungen und Dienste, die über das Internet zugänglich sind. Kunden nutzen SaaS-Anwendungen, ohne sich um die Infrastruktur oder Wartung zu kümmern. Beispiele für SaaS sind E-Mail-Dienste wie Gmail und Unternehmenssoftware wie Salesforce.
