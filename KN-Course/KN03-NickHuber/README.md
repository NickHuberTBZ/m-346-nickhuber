## KN03 - Nick Huber Dokumentation

[TOC]

### A) Cloud-init verstehen

Um diese Aufgabe zu lösen musste man ein Beispiel File beschreiben, dies habe ich [hier](https://gitlab.com/NickHuberTBZ/m-346-nickhuber/-/blob/main/KN-Course/KN03-NickHuber/cloud-init.yaml?ref_type=heads) gemacht.

### B) SSH-Key und Cloud-init

Um beide public-Keys ohne GUI zu extrahieren ist hier der Befehl für Windows. 

``` 
ssh-keygen -y -f C:/Users/nickr/.ssh/nick-1.pem > C:/Users/nickr/.ssh/nick-1.pub

ssh-keygen -y -f C:/Users/nickr/.ssh/nick-2.pem > C:/Users/nickr/.ssh/nick-2.pub
```

Bei mir hat das Verbinden mit beiden Keys nicht funktioniert. Beim Trouble-Shooting hat mit @LucaRamonSuter geholfen aber es hat nach 2 Stunden versuchen immernoch nicht funktioniert.

Damit ich dann die Screenshots von dem Cloud-init Log bekommen habe, habe ich mich einfach über die Browser Konsole verbunden.

### C) Template

Hier ist die fertig bearbeitete Datei: [cloud-init-lehrer.yaml](https://gitlab.com/NickHuberTBZ/m-346-nickhuber/-/blob/main/KN-Course/KN03-NickHuber/cloud-init-lehrer.yaml?ref_type=heads)

### D) Auftrennung von Web- und Datenbankserver

Als erstes habe ich die zwei Instazen aufgesetzt wie vorgegeben. Die Cloud-init Datein habe ich mit Hilfe von Chatgpt geschrieben. Da ich bei einigen Befehlen nichts im Internet gefunden habe. Danach habe immer die Schritte befolgt bis ich auf den Fehler gestossen bin das ich die Ports nicht definiert hatte. Diese habe ich dann mit Hilfe gesetzt, danach hat es funktioniert. 

Die Schritte mit dem Runcmd und write-files wurden extra erwähnt, darum habe ich sie hier noch hingeschrieben:

```packages:```

```yaml
packages:
  - mariadb-server
```

```yaml
packages:
  - apache2
  - php
  - libapache2-mod-php
  - php-mysqli
  - adminer
``` 

``` runcmd: ```

```yaml
runcmd:
  - [sudo, mysql, -sfu, root, -e, "GRANT ALL ON *.* TO 'admin'@'%' IDENTIFIED BY 'password' WITH GRANT OPTION;"]
  - [sudo, sed, -i, 's/127.0.0.1/0.0.0.0/g', /etc/mysql/mariadb.conf.d/50-server.cnf]
  - [sudo, systemctl, restart, mariadb.service]#
  ```

  ```write-files:```

```yaml
write_files:
  - path: /var/www/html/db.php
    content: |
      <?php
          //database
          $servername = "54.174.46.247";
          $username = "admin";
          $password = "password";
          $dbname = "mysql";

          // Create connection
          $conn = new mysqli($servername, $username, $password, $dbname);
          // Check connection
          if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
          }

          $sql = "select Host, User from mysql.user;";
          $result = $conn->query($sql);
          while($row = $result->fetch_assoc()){
                  echo($row["Host"] . " / " . $row["User"] . "<br />");
          }
          //var_dump($result);
      ?>
  - path: /var/www/html/info.php
    content: |
      <?php

      // Show all information, defaults to INFO_ALL
      phpinfo();

      ?>
```


