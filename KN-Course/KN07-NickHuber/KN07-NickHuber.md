# KN07

## Warum AWS:

![AWS](AWS-Server.png)

Ich habe mich für normale EC2 Instanzen entschieden, auch wenn es spezifischere Möglichkeiten gab, diese aber jedoch teurer sind. Man kann auf einer EC2 Instanz das gewünschte Programm einfach installieren. Ich habe die EC2 Instanzen so konfiguriert, wie es gewünscht wurde. Zusätzlich habe ich die gewünschten Services hinzugefügt wie Backup und Load Balancer, denn man kann in der Instanz kein Backup konfigurieren, deshalb muss man es hinzufügen. Ich habe mich beim Backup für den S3 Bucked entschieden, da er mir vertraut vorkam.

## Warum Azure

![Azure](Azure-Server.png)

Hier habe ich mich für den Webserver für den Azure App Service entschieden, da man den auch mit Linux haben kann und dort die gewünschten Webserver Applikationen installieren. Für den Datenbankserver aber ich mich für den Azure Database for MySQL entschieden da dieser nicht viel teurer ist als der App Service mit den gewünschten Konfigurationen. Mann kann zwar bei der MySQL Datenbank ein Backup konfigurieren, jedoch nicht mit den gewünschten Anforderungen, deshalb habe ich wieder einen zusätzlichen Backup-Service genommen.

## Warum Heroku

![Heruko](Heruko.png)

Hier kann man einfach die Plattform auswählen und es kommen nur zwei Typen infrage, Produkcion oder Advanced. Bei Advanced ist jedoch bei Dynos die Performance Auswahl von den specs passend, aber sehr teuer. Zudem, wenn man dann den Datenbankserver will, muss man nachher bei Postgres auch das Premium nehmen und um die Anforderungen zu erfüllen, das Premium 2 nehmen, welches alleine schon 250 kostet. Aber bei Heroku sind die Instanzen skalierbar, deshalb habe ich mich dafür entschieden, die Production Kategorie zu wählen und dafür zweimal den Standard 2X für den Webserver und zweimal den Standard 2 für den Datenbankserver. Wenn man die Konfiguration so übernimmt, spart man 200. Zudem kann man den Load Balancer nicht konfigurieren und Backup, Überwachung und Sicherheit ist schon dabei bei dem Datenbankserver.

## Warum Zoho

![Zoho](Zoho.png)

Dann bei Zoho wählt man dann einfach die Software aus. Hier habe ich mich für die Enterprise entschieden, weil sie günstiger ist als die Ultimate Version, aber trotzdem das notwendige deckt. Bei Enterprise sind so Spezialprogramme wie KI, Sandbox, Marketingautomatisierungen, mehr Arten der Analyse und Messung und es sind viele Entwicklertools dabei welche nicht bei den unteren Stufen dabei sind, zudem hat man eine grosse Auswahl bei der Produktanpassung. Ganz wichtig, es ist auch noch eine kostenlose Testversion dabei. Zudem gibt es die Möglichkeit für eine jährliche Abrechnung, bei welcher man nochmal etwa 2300, spart, ich würde das nehmen, weil es auch eine Testversion gibt, um zu schauen, ob es sich lohnt das Jahresabo zu nehmen, aber man kann dort einfach monatlich kündigen.

## Warum SalesForce

![Sales](Sales.png)
![Abrechung](SalesAbrechung.png)

Bei Salesforce kann man keine genaue Specs bei den Abos anschauen, aber man zahlt dafür viel mehr. Ich habe keine Informationen zu den einzelnen Abos gefunden und kann diese deshalb schwer vergleichen. Aber wenn man das mit Zoho vergleicht, wenn man eine API einbinden will, muss man schon Enterprise nehmen, welches schon 165 pro Benutzer, pro Monat kostet. Wenn man einer AI will, ähnlich wie die KI bei Zoho muss man schon 330 pro Benutzer, pro Monat zahlen.

## IAAS, PAAS doer SAAS

Ich entscheide mich für PaaS, weil mir IaaS zu kompliziert ist und man sehr viel Aufwand braucht, zudem kommen noch die Betriebskosten der Instanzen dazu. Bei SAAS hat man dafür zu wenig Freiheit und es kostet auch viel mehr als die andere. Bei PaaS hingegen muss man sich nicht mehr um das Interface kümmern, hat jedoch den Vorteil, dass die Instanzen skalierbar sind und man viel mehr Freiheit hat bei der Software hingegen zum SAAS und es ist auch viel billiger. Man muss bei PaaS beachten, dass man die Instanz sorgfältig und richtig auswählt.

## Unterschiede

Man merkt, dass es immer teuer wird, desto mehr vom Anbieter schon bereits zur Verfügung gestellt wird. Bei AWS und Azure zahlt man am wenigstens, man muss sich dafür auch un alles kümmern. Bei Heroku zahlt man schon deutlich mehr, dafür muss man sich nicht mehr um die Instanzen und die Infrastruktur kümmern. Bei Zogo und Salesforce zahlt man schlieslich am meisten, muss sichh dafür am wenigsten Gedanken machen.
* Bei AWS zaht man am wenigstens für ihren Service nähmlich nur 67 Dollar im Monat dafür muss man sich um alles kümmern, die Instanzen richtig aufsetzen und das braucht auch Arbeiter die kosten.
* Bei Azure zahlt man schon ein bisschen mehr, dafür bekommt man direkt eine DB Server Instanzen welche man nicht installieren muss und der Load Balancer ist gratis, dafür zahlt man viel mehr für das Backup.
* Bei Heruko zahlt man schon mehr, dafür muss man die Instanzen nicht mehr aufsetzen und wenn man die Services sorgfältig auswählt zahl man 200 dollar für das nötige. Das Backup, Sicherheit und Überwachung der Instanzenist schon dabei, wo man wieder Geld spart.
* Bei Zoho zahlt man, jenachdem was man will, 800 bei monatlicher Abrechung und 640 bei jährlicher Abrechung, man kann dafür nicht monatlich kündigen, man hat aber eine kostenlose Testversion um zu schauen ob es das richtige ist oder nicht. Zusem muss man sich nicht mehr um die Instanzen und die Plattform kümmern wo man auch wieder Geld sparen könnte.
* Bei Salesforce zahlt man, welche Kategorie man will von 400 Dollar, 1280 Dollar, 2640 Dollar bis 5280 Dollar im Monat. Man findet nicht viel zu den Specs der Services.

## Günstigster

Von den Zahlen her ist der AWS am günstigsten und der Azure auch. Doch ich finde wegen den Überwachungs und Betriebskosten der Instanzen sie Heruko lösung am günstigsten da man sich dort nicht um die Bachups, Überwachung und Maintainance der Instanzen kümmern was im Endefekt günstiger ist als AWS.

## Warum teurer

Wenn man die Preise so jetzt anschaut merkt man, dass das Salesforce sehr viel teurer ist als die anderen. Zoho ist zwar günstiger, aber auch wieder nicht günstiger als die anderen beiden Lösungen. Auf den ersten Blick denkt man sicher, dass Salesforce und Zoho sicher viel zu teuer sind und die anderen viel günstiger. Jedoch muss man beachten, dass man bei Azure und AWS nur die Infrastruktur zur Verfügung gestellt bekommt, also muss man sich selber um die Plattform und die Software kümmern was auch wieder Entwickler braucht und diese kosten. Auch kostet die Überwachung und Maintainance der Tools augh nochmal. Bei Heruko bekommt man zwar die Infrastruktur schon muss sich aber immernoch um die Software kümmern, was auch Entwickler braucht die diese Software machen und danach noch maintainen. Wenn man das jetzt alles zusammen nimmt, sind die ertsen zwei Optionen nicht einmal viel günstiger als Zoho aber Salesforce wird vermutlich immernoch sehr viel teuer sein.