## KN02 - Nick Huber Dokumentation

[TOC]

### A) Lab 4.1 - EC2 und S3

#### Schritt 1: EC2-Instanz starten

1. Als ersten Schritt, um eine EC2-Instanz (bezogen auf Learner Lab 4.1 - EC2) zu erstellen, habe ich mich im Lab 4.1 angemeldet.

2. Nachdem ich mich angemeldet habe, bin ich oben rechts auf "Services" gegangen und habe "EC2" unter der Kategorie "Compute" ausgewählt.

3. In der EC2-Konsole habe ich auf die Schaltfläche "Instanzen starten" oder "Instanz starten" geklickt, um eine neue EC2-Instanz zu erstellen.

#### Schritt 2: EC2-Instanz konfigurieren

4. Ich habe einen AMI (Amazon Machine Image) aus der Liste der verfügbaren Images ausgewählt. Ich konnte entweder ein vorhandenes Image verwenden oder ein neues erstellen. Anschließend habe ich auf "Weiter" geklickt, um fortzufahren.

5. Dann habe ich den gewünschten Instanztyp ausgewählt, der meinen Anforderungen entsprach, und erneut auf "Weiter" geklickt.

6. Die Instanzdetails waren bereits vorgegeben, einschließlich der Anzahl der Instanzen, Netzwerkoptionen und Startskripte. Ich habe auf "Weiter" geklickt.

7. Die Speicherplatzanforderungen für meine Instanz habe ich festgelegt, indem ich EBS-Volumes hinzugefügt oder vorhandene verwendet habe. Danach habe ich auf "Weiter" geklickt.

8. Ich habe die Sicherheitsgruppen konfiguriert, um den Zugriff auf meine Instanz zu steuern, indem ich Regeln für den eingehenden Datenverkehr (Inbound Rules) erstellt habe, um den Zugriff auf bestimmte Ports zuzulassen. Anschließend habe ich auf "Überprüfen und starten" geklickt.

9. Nachdem ich meine Konfiguration überprüft habe, habe ich auf "Starten" geklickt, um die Instanz zu starten.

10. Ich konnte entweder ein vorhandenes Schlüsselpaar auswählen oder ein neues Schlüsselpaar erstellen, um auf die Instanz zuzugreifen. Danach habe ich auf "Starten Sie Instanzen" geklickt.

#### Schritt 3: EC2-Instanz anzeigen und Screenshots erstellen

11. Nachdem ich die Instanz gestartet habe, bin ich zur EC2-Konsole zurückgekehrt.

12. In der EC2-Konsole konnte ich die Liste der EC2-Instanzen anzeigen, die ich erstellt habe, und habe einen Screenshot dieser Liste erstellt.

13. Ich habe auf die gerade erstellte EC2-Instanz geklickt, um weitere Details anzuzeigen, einschließlich der öffentlichen IP-Adresse, und habe einen Screenshot dieser Detailansicht erstellt.

14. Ich habe die Sicherheitsgruppen überprüft, indem ich auf die Instanz geklickt habe und auf die Registerkarte "Sicherheitsgruppen" gegangen bin. Dann habe ich einen Screenshot der Liste der Inbound-Regeln erstellt.

#### Schritt 4: Abschluss

15. Nachdem ich alle Schritte abgeschlossen und Screenshots erstellt hatte, habe ich die Screenshots in einem geeigneten Format (z. B. PNG) auf meinem lokalen Computer gespeichert.


Die Screenshots finden sie [hier](https://gitlab.com/NickHuberTBZ/m-346-nickhuber/-/tree/main/KN-Course/KN02-NickHuber/Bilder).

Das war die Dokumentation für den ersten Punkt ("Lab 4.1 - EC2"). Für den zweiten Punkt ("Lab 4.2 - S3") kann man ähnliche Schritte befolgen, um S3-Buckets und deren Eigenschaften zu konfigurieren und Screenshots zu erstellen.

### B) Zugriff mit SSH-Key

#### Schritt 1: Einlesen in SSH-Schlüssel

Ich habe begonnen, indem ich mich in das Dokument eingearbeitet habe, das die Unterschiede zwischen Passwörtern und SSH-Schlüsseln beschreibt. Dabei habe ich festgelegt, dass wir in diesem Fall ausschließlich SSH-Schlüssel verwenden werden.

#### Schritt 2: Starten einer neuen Ubuntu-Instanz in AWS

In meiner AWS-Konsole habe ich eine neue Ubuntu-Instanz gestartet, wobei ich die Standard-Einstellungen verwendet habe. Wichtig war, dass ich zwei Key-Value-Paare unter "Key pair" erstellt habe und diese heruntergeladen habe. Ich habe den Namen für diese Schlüsselpaare in der Form "<Ihr-Name>-1" und "<Ihr-Name>-2" festgelegt. Um fortzufahren, habe ich den ersten Schlüssel ausgewählt und die Instanz gestartet.

#### Schritt 3: Speicherort für die Schlüssel

Ich habe die privaten und öffentlichen Schlüssel an einem Ort mit eingeschränkten Zugriffsrechten gespeichert. Normalerweise verwende ich den Pfad "C:\Users\<IhrBenutzer>\.ssh" auf Windows.

#### Schritt 4: SSH-Zugriff auf die Instanz

Um SSH-Verbindungen herzustellen, habe ich den folgenden Befehl verwendet, um sicherzustellen, dass kein Timeout auftritt:

```css
 ssh -i "nick-1.pem" ubuntu@ec2-3-85-11-39.compute-1.amazonaws.com
```

Ich habe denselben Befehl für den zweiten Schlüssel ausgeführt. 

#### Schritt 5: Dokumentation

Für die Dokumentation habe ich Screenshots von jedem der folgenden Schritte erstellt:

1. Ich habe einen Screenshot des SSH-Befehls und des Ergebnisses unter Verwendung des ersten Schlüssels gemacht.
2. Ebenso habe ich einen Screenshot des SSH-Befehls und des Ergebnisses unter Verwendung des zweiten Schlüssels erstellt.
3. Schließlich habe ich einen Screenshot der Instanz-Detail oder -Liste in AWS erstellt, auf dem der verwendete Schlüssel sichtbar ist.

Zusätzlich habe ich Erklärungen zu den Screenshots hinzugefügt, um den Prozess zu beschreiben und sicherzustellen, dass mein Vorgehen und die Ergebnisse klar verständlich sind.

Es ist wichtig zu beachten, dass der private Schlüssel nicht verloren gehen darf, da AWS ihn nicht wiederherstellen kann. Auf dem Ubuntu-Server liegt immer nur der öffentliche Schlüssel. Daher ist Vorsicht geboten, um die Schlüssel sicher aufzubewahren.

### C) Installation von Web- und Datenbankserver

#### Bild der fertigen Website

![Bild](Bilder/InstanceWebsiteIndexHtml.png)

